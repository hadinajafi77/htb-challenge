# HTB OpeningHours Challenge

Has To Be code challenge solves current status 
of charging stations and next upcoming status
changes.

- First install maven:
`sudo apt install maven`.
- Then build the project and produce jar file with
`mvn clean package`.
- After build completion, we start to build the docker image:
`sudo docker build -t htb-challenge`.
- Then we run the docker image with this command:
`sudo docker run -p 9090:8080 htb-challenge`.

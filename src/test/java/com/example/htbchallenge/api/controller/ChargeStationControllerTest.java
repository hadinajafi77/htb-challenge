package com.example.htbchallenge.api.controller;

import com.example.htbchallenge.api.controller.chargingstation.dto.ChargeStationStatusReq;
import com.example.htbchallenge.common.Constants.PathUrls;
import com.example.htbchallenge.common.exceptions.ResourceNotFoundException;
import com.example.htbchallenge.persistance.entity.*;
import com.example.htbchallenge.persistance.service.chargingstation.ChargingStationService;
import com.example.htbchallenge.persistance.service.exception.ExceptionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class ChargeStationControllerTest {

	@MockBean
	private ChargingStationService stationService;
	@MockBean
	private ExceptionService exceptionService;
	@Autowired
	private MockMvc mockMvc;

	private final static String BASE_URL = PathUrls.V1 + PathUrls.CHARGING_STATION_BASE;
	private final ChargingStationEntity stationEntity = new ChargingStationEntity();
	private final StoreEntity storeEntity = new StoreEntity();
	private final TenantEntity tenantEntity = new TenantEntity();
	private final UUID stationId = UUID.randomUUID();
	private final UUID storeId = UUID.randomUUID();
	private final ChargeStationStatusReq statusReq = new ChargeStationStatusReq();

	@BeforeEach
	void init() {
		setFakeStationProperties();
		setFakeStoreProperties();
		setFakeTenantProperties();
	}

	@Test
	@SneakyThrows
	void shouldThrowNotFoundException() {
		final var uuid = UUID.randomUUID();
		doThrow(new ResourceNotFoundException("Item not found")).when(stationService).fetch(stationId);
		mockMvc.perform(post(BASE_URL + "/" + stationId)
						.content(new ObjectMapper().writeValueAsString(statusReq))
						.contentType("application/json"))
				.andExpect(status().isNotFound());
	}

	@Test
	@SneakyThrows
	void shouldReturnChargingStationExceptionStatus() {
		final var exception = createFakeException();
		stationEntity.setExceptions(List.of(exception));

		doReturn(stationEntity).when(stationService).fetch(stationId);
		doReturn(exception).when(exceptionService).fetch(exception.getId());
		doReturn(exception.getStatus()).when(exceptionService).checkStatus(exception.getId(), statusReq.getTimestamp());

		mockMvc.perform(post(BASE_URL + "/" + stationId)
						.content(new ObjectMapper().writeValueAsString(statusReq))
						.contentType("application/json"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", Matchers.is("CLOSED")));
	}

	@Test
	@SneakyThrows
	void shouldReturnStoreExceptionStatus() {
		final var exception = createFakeException();
		storeEntity.setExceptions(List.of(exception));

		doReturn(stationEntity).when(stationService).fetch(stationId);
		doReturn(exception).when(exceptionService).fetch(exception.getId());
		doReturn(exception.getStatus()).when(exceptionService).checkStatus(exception.getId(), statusReq.getTimestamp());

		mockMvc.perform(post(BASE_URL + "/" + stationId)
						.content(new ObjectMapper().writeValueAsString(statusReq))
						.contentType("application/json"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", Matchers.is("CLOSED")));
	}

	@Test
	@SneakyThrows
	void shouldReturnTenantExceptionStatus() {
		final var exception = createFakeException();
		tenantEntity.setExceptions(List.of(exception));

		doReturn(stationEntity).when(stationService).fetch(stationId);
		doReturn(exception).when(exceptionService).fetch(exception.getId());
		doReturn(ExceptionEntity.Status.OPEN).when(exceptionService).checkStatus(exception.getId(), statusReq.getTimestamp());

		mockMvc.perform(post(BASE_URL + "/" + stationId)
						.content(new ObjectMapper().writeValueAsString(statusReq))
						.contentType("application/json"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", Matchers.is("OPEN")));
	}

	@Test
	@SneakyThrows
	void shouldReturnOpeningHoursWhenThereIsNoException() {
		doReturn(stationEntity).when(stationService).fetch(stationId);
		doReturn(ChargingStationEntity.Status.OPEN).when(stationService)
				.checkOpeningHour(stationEntity, statusReq.getTimestamp());

		mockMvc.perform(post(BASE_URL + "/" + stationId)
						.content(new ObjectMapper().writeValueAsString(statusReq))
						.contentType("application/json"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", org.hamcrest.Matchers.is("OPEN")));
	}

	@Test
	@SneakyThrows
	void findNextOpenCloseTimestamp() {
		var timestamp = new Timestamp(650000000L);

		doReturn(stationEntity).when(stationService).fetch(stationId);
		doReturn(timestamp).when(stationService).getNextStatusTime(stationEntity, statusReq.getTimestamp());

		mockMvc.perform(post(BASE_URL + "/" + stationId + PathUrls.NEXT_STATUS)
						.content(new ObjectMapper().writeValueAsString(statusReq))
						.contentType("application/json"))
				.andExpect(status().isOk());
	}

	private ExceptionEntity createFakeException() {
		var exception = new ExceptionEntity();
		exception.setId(UUID.randomUUID());
		exception.setStation(stationEntity);
		exception.setStore(storeEntity);
		exception.setStatus(ExceptionEntity.Status.CLOSED);
		return exception;
	}

	private void setFakeStoreProperties() {
		storeEntity.setTitle("Bingo");
		storeEntity.setChargingStations(List.of(stationEntity));
		storeEntity.setId(storeId);
		storeEntity.setTenant(tenantEntity);
	}

	private void setFakeStationProperties() {
		stationEntity.setId(stationId);
		var dayTime = createBusinessPeriod(new Timestamp(15000000L), new Timestamp(15010000L));
		stationEntity.setOpeningHours(Set.of(dayTime));
		stationEntity.setStore(storeEntity);
	}

	private BusinessDayEntity createBusinessPeriod(Timestamp start, Timestamp close) {
		var entity = new BusinessDayEntity();
		entity.setStartTime(start);
		entity.setEndTime(close);
		return entity;
	}

	private void setFakeTenantProperties() {
		tenantEntity.setId(UUID.randomUUID());
		tenantEntity.setName("Total");
		tenantEntity.setStore(List.of(storeEntity));
	}

}
package com.example.htbchallenge.persistance.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class ChargingStationEntityTest {

	private ChargingStationEntity entity;

	@BeforeEach
	void init() {
		entity = new ChargingStationEntity();
	}

	@Test
	void setIdShouldWork() {
		entity.setId(UUID.randomUUID());
		assertNotNull(entity.getId());
		assertDoesNotThrow(() -> UUID.fromString(entity.getId().toString()));
	}

	@Test
	void setStoreEntityShouldWork() {
		var store = new StoreEntity();
		store.setTitle("title");
		entity.setStore(store);
		assertEquals("title", entity.getStore().getTitle());
	}

	@Test
	void setOpeningHoursShouldWork() {
		var hours = new BusinessDayEntity();
		hours.setStartTime(new Timestamp(1000L));
		hours.setEndTime(new Timestamp(100000L));
		entity.setOpeningHours(Set.of(hours));
		assertEquals(1, entity.getOpeningHours().size());
	}

}
package com.example.htbchallenge.persistance.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class TenantEntityTest {
	private TenantEntity entity;

	@BeforeEach
	void init() {
		entity = new TenantEntity();
	}

	@Test
	void setIdShouldWork() {
		entity.setId(UUID.randomUUID());
		assertNotNull(entity.getId());
		assertDoesNotThrow(() -> UUID.fromString(entity.getId().toString()));
	}


	@Test
	void setNameShouldWork() {
		entity.setName("Eagle Corp");
		assertEquals(entity.getName(), "Eagle Corp");
	}

	@Test
	void setEmailShouldWork() {
		entity.setEmail("sample@mail.com");
		assertEquals(entity.getEmail(), "sample@mail.com");
	}

	@Test
	void setExpirationShouldWork() {
		entity.setExpired(true);
		assertTrue(entity.getExpired());
	}

	@Test
	void setStoreListShouldWork() {
		entity.setStore(List.of(new StoreEntity()));
		assertEquals(1, entity.getStore().size());
	}
}
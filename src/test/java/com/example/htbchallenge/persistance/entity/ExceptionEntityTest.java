package com.example.htbchallenge.persistance.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class ExceptionEntityTest {
	private ExceptionEntity entity;

	@BeforeEach
	void init() {
		entity = createFakeEntity();
	}

	@Test
	void descriptionAndStatusShouldBeCorrect() {
		assertEquals(entity.getDescription(), "sample");
		assertEquals(entity.getStatus(), ExceptionEntity.Status.CLOSED);
	}

	@Test
	void settingStoreShouldWork() {
		var store = new StoreEntity();
		store.setTitle("store");
		entity.setStore(store);
		assertEquals("store", entity.getStore().getTitle());
	}

	@Test
	void settingTenantShouldWork() {
		var tenant = new TenantEntity();
		tenant.setName("Audio");
		entity.setTenant(tenant);
		assertEquals("Audio", entity.getTenant().getName());
	}

	@Test
	void settingStationShouldWork() {
		var station = new ChargingStationEntity();
		var id = UUID.randomUUID();
		station.setId(id);
		entity.setStation(station);
		assertEquals(id, entity.getStation().getId());
	}

	private ExceptionEntity createFakeEntity() {
		var entity = new ExceptionEntity();
		entity.setDescription("sample");
		entity.setStatus(ExceptionEntity.Status.CLOSED);
		entity.setId(UUID.randomUUID());
		return entity;
	}


}
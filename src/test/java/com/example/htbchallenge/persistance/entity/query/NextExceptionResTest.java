package com.example.htbchallenge.persistance.entity.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class NextExceptionResTest {

	private NextExceptionRes result;

	@BeforeEach
	void init() {
		result = new NextExceptionRes(UUID.randomUUID(), new Date());
	}

	@Test
	void settingExceptionId() {
		var id = UUID.randomUUID();
		result.setExceptionId(id);
		assertEquals(id, result.getExceptionId());
	}

	@Test
	void settingMinTimeDate(){
		var time = new Timestamp(3424242L);
		result.setMinTime(time);
		assertEquals(time, result.getMinTime());
	}
}
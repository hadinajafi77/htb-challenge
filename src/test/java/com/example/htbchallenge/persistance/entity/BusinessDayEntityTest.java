package com.example.htbchallenge.persistance.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.sql.Timestamp;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class BusinessDayEntityTest {

	private BusinessDayEntity entity;

	@BeforeEach
	void init() {
		entity = createFakeEntity();
	}

	private BusinessDayEntity createFakeEntity() {
		var entity = new BusinessDayEntity();
		entity.setEndTime(new Timestamp(100L));
		entity.setStartTime(new Timestamp(40L));
		return entity;
	}

	@Test
	void settingStartAndEndTimeShouldWork() {
		entity.setStartTime(new Timestamp(200));
		assertEquals(200L, entity.getStartTime().getTime());
		entity.setEndTime(new Timestamp(4000L));
		assertEquals(4000L, entity.getEndTime().getTime());
	}

	@ParameterizedTest
	@ValueSource(longs = {41, 50, 90, 99})
	void isInPeriodTest(Long param) {
		assertTrue(entity.isInPeriod(new Timestamp(param)));
	}

	@ParameterizedTest
	@ValueSource(longs = {40, 100, 101, 2000, 30, 67000000})
	void isNotInPeriodTest(Long param) {
		assertFalse(entity.isInPeriod(new Timestamp(param)));
	}

	@Test
	void settingStationShouldWork() {
		var station = new ChargingStationEntity();
		station.setOpeningHours(Set.of(entity));
		entity.setStation(station);
		assertEquals(entity.getStation().getOpeningHours().size(), station.getOpeningHours().size());
	}

	@Test
	void settingTypeCorrectlyShouldWork() {
		entity.setType(BusinessDayEntity.Type.BASIC);
		assertEquals(BusinessDayEntity.Type.BASIC, entity.getType());
	}

	@Test
	void settingInvalidTypeShouldThrowError() {
		assertThrows(IllegalArgumentException.class, () -> entity.setType(BusinessDayEntity.Type.valueOf("Custom")));
	}

	@Test
	void settingExceptionEntityShouldWork() {
		entity.setException(new ExceptionEntity());
		assertNotNull(entity.getException());
	}

}
package com.example.htbchallenge.persistance.entity.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NexOpeningHourResTest {
	private NexOpeningHourRes entity;

	@BeforeEach
	void init() {
		entity = new NexOpeningHourRes();
		entity.setBusinessDayId(UUID.randomUUID());
		entity.setStationId(UUID.randomUUID());
		entity.setMinStartTime(new Timestamp(123400000L));
	}

	@Test
	void testSettingBusinessDayId() {
		var id = UUID.randomUUID();
		entity.setBusinessDayId(id);
		assertEquals(entity.getBusinessDayId(), id);
	}

	@Test
	void testSettingStationId() {
		var id = UUID.randomUUID();
		entity.setStationId(id);
		assertEquals(id, entity.getStationId());
	}

	@Test
	void testSettingMinStartTime() {
		var time = new Timestamp(200000L);
		entity.setMinStartTime(time);
		assertEquals(entity.getMinStartTime(), time);
	}

}
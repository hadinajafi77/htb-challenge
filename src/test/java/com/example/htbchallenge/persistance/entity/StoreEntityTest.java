package com.example.htbchallenge.persistance.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class StoreEntityTest {
	private StoreEntity entity;

	@BeforeEach
	void init() {
		entity = new StoreEntity();
	}

	@Test
	void setIdShouldWork() {
		entity.setId(UUID.randomUUID());
		assertNotNull(entity.getId());
		assertDoesNotThrow(() -> UUID.fromString(entity.getId().toString()));
	}

	@Test
	void setTitleShouldWork() {
		entity.setTitle("FreeShop");
		assertEquals("FreeShop", entity.getTitle());
	}

	@Test
	void setTenantShouldWork() {
		var tenant = new TenantEntity();
		tenant.setName("Sample");
		entity.setTenant(tenant);
		assertEquals("Sample", entity.getTenant().getName());
	}

	@Test
	void setChargingStationShouldWork() {
		entity.setChargingStations(List.of(new ChargingStationEntity(), new ChargingStationEntity()));
		assertEquals(2, entity.getChargingStations().size());
	}

}
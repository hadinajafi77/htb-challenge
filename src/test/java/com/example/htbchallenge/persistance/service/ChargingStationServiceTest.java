package com.example.htbchallenge.persistance.service;

import com.example.htbchallenge.common.exceptions.ResourceNotFoundException;
import com.example.htbchallenge.persistance.entity.*;
import com.example.htbchallenge.persistance.entity.ChargingStationEntity.Status;
import com.example.htbchallenge.persistance.entity.query.NexOpeningHourRes;
import com.example.htbchallenge.persistance.repository.BusinessDayRepository;
import com.example.htbchallenge.persistance.repository.ChargingStationRepository;
import com.example.htbchallenge.persistance.service.chargingstation.ChargingStationService;
import com.example.htbchallenge.persistance.service.chargingstation.ChargingStationServiceImpl;
import com.example.htbchallenge.persistance.service.exception.ExceptionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ChargingStationServiceTest {

	@Mock
	private ChargingStationRepository repository;
	@Mock
	private ExceptionService exceptionService;
	@Mock
	private BusinessDayRepository businessDayRepository;
	private ChargingStationService service;
	private StoreEntity store;
	private ChargingStationEntity station;
	private ExceptionEntity exception;
	private TenantEntity tenant;
	private BusinessDayEntity period;
	private final UUID id = UUID.randomUUID();

	@BeforeEach
	void init() {
		period = createBusinessPeriod();
		exception = createFakeException();
		station = createFakeStation();
		store = createFakeStore();
		tenant = createFakeTenant();
		service = new ChargingStationServiceImpl(repository, exceptionService, businessDayRepository);
		Mockito.lenient().doReturn(Optional.of(station)).when(repository).findById(id);
	}

	@ParameterizedTest
	@ValueSource(longs = {15000000, 15010000, 16000000, 14000000, 14999999, 10, 65000000})
	void checkStatusWithInvalidInputShouldReturnClosed(Long timestamp) {
		var status = service.checkOpeningHour(station, new Timestamp(timestamp));
		assertEquals(Status.CLOSED, status);
	}

	@ParameterizedTest
	@ValueSource(longs = {3001, 3500, 3999})
	void checkStatusWithValidInputShouldReturnOpen(Long timestamp) {
		var status = service.checkOpeningHour(station, new Timestamp(timestamp));
		assertEquals(Status.OPEN, status);
	}

	@Test
	void fetchNotExistedStationShouldThrowResourceNotFoundException() {
		Mockito.lenient().doReturn(Optional.empty()).when(repository).findById(id);
		assertThrows(ResourceNotFoundException.class, () -> service.fetch(id));
	}

	@Test
	void fetchExistingEntityShouldWork() {
		assertDoesNotThrow(() -> service.fetch(id));
	}

	/**
	 * Should return next station <b>Close time </b> since it's between opening hour.
	 * exception period is 1000-2000 and openingHour period is 0-900.
	 * @param param random numbers lower that exception and in range of opening hour periods
	 */
	@ParameterizedTest
	@ValueSource(longs = {100, 1, 500, 899})
	void shouldReturnBusinessDayCloseTime(Long param) {
		var timestamp = new Timestamp(param);
		station.setStore(store);
		store.setTenant(tenant);
		var businessHour = createBusinessPeriod();
		businessHour.setStartTime(new Timestamp(0));
		businessHour.setEndTime(new Timestamp(900));
		station.setOpeningHours(Set.of(businessHour));

		var nxOpHourDto = new NexOpeningHourRes(period.getId(), station.getId(), period.getStartTime());

		Mockito.lenient().doReturn(exception).when(exceptionService)
				.findNextExceptionByStationId(timestamp, station.getId());
		Mockito.lenient().doReturn(null).when(exceptionService)
				.findNextExceptionByStoreId(timestamp, store.getId());
		Mockito.lenient().doReturn(null).when(exceptionService)
				.findNextExceptionByTenantId(timestamp, tenant.getId());
		Mockito.lenient().doReturn(Optional.of(businessHour)).when(businessDayRepository).findById(businessHour.getId());
		Mockito.lenient().doReturn(List.of(nxOpHourDto)).when(repository).findNextOpeningHourByStationId(timestamp, station.getId());

		assertEquals(businessHour.getEndTime(), service.getNextStatusTime(station, timestamp));
	}

	/**
	 * Should return next openingHour start time, since it happens sooner that next exception.
	 * exception period is 1000-2000 and opening hour is 500-900.
	 * @param param numbers lower than exception & business day period.
	 */
	@ParameterizedTest
	@ValueSource(longs = {100, 0, 400, 500})
	void shouldReturnNextBusinessOpenTime(Long param) {
		var timestamp = new Timestamp(param);
		station.setStore(store);
		store.setTenant(tenant);
		var businessHour = createBusinessPeriod();
		businessHour.setStartTime(new Timestamp(500));
		businessHour.setEndTime(new Timestamp(900));
		station.setOpeningHours(Set.of(businessHour));
		var nxOpHourDto = new NexOpeningHourRes(id, station.getId(), period.getStartTime());

		Mockito.lenient().doReturn(exception).when(exceptionService)
				.findNextExceptionByStationId(timestamp, station.getId());
		Mockito.lenient().doReturn(null).when(exceptionService)
				.findNextExceptionByStoreId(timestamp, store.getId());
		Mockito.lenient().doReturn(null).when(exceptionService)
				.findNextExceptionByTenantId(timestamp, tenant.getId());
		Mockito.lenient().doReturn(Optional.of(businessHour)).when(businessDayRepository).findById(businessHour.getId());
		Mockito.lenient().doReturn(List.of(nxOpHourDto)).when(repository).findNextOpeningHourByStationId(timestamp, station.getId());

		assertEquals(businessHour.getStartTime(), service.getNextStatusTime(station, timestamp));
	}

	/**
	 * Should return next exception start time, since it happens sooner that next openingHour close time.
	 * exception period is 1000-2000 and opening hour is 1500-3000
	 * @param param test parameters
	 */
	@ParameterizedTest
	@ValueSource(longs = {100, 0, 400, 500})
	void shouldReturnExceptionStartTime(Long param) {
		var timestamp = new Timestamp(param);
		station.setStore(store);
		store.setTenant(tenant);
		var businessHour = createBusinessPeriod();
		businessHour.setStartTime(new Timestamp(1500));
		businessHour.setEndTime(new Timestamp(3000));
		station.setOpeningHours(Set.of(businessHour));
		var nxOpHourDto = new NexOpeningHourRes(id, station.getId(), businessHour.getStartTime());

		Mockito.lenient().doReturn(exception).when(exceptionService)
				.findNextExceptionByStationId(timestamp, station.getId());
		Mockito.lenient().doReturn(null).when(exceptionService)
				.findNextExceptionByStoreId(timestamp, store.getId());
		Mockito.lenient().doReturn(null).when(exceptionService)
				.findNextExceptionByTenantId(timestamp, tenant.getId());
		Mockito.lenient().doReturn(Optional.of(businessHour)).when(businessDayRepository).findById(businessHour.getId());
		Mockito.lenient().doReturn(List.of(nxOpHourDto)).when(repository).findNextOpeningHourByStationId(timestamp, station.getId());

		assertEquals(exception.getPeriod().getStartTime(), service.getNextStatusTime(station, timestamp));
	}

	/**
	 * Should return next exception start time, since it happens sooner that next openingHour
	 * exception period is 1000-2000 and opening hour is 1500-2000
	 * @param param numbers lower than exception & business day period
	 */
	@ParameterizedTest
	@ValueSource(longs = {900, 600})
	void shouldReturnExceptionStartTimeAndBeforeBusinessStart(Long param) {
		var timestamp = new Timestamp(param);
		station.setStore(store);
		store.setTenant(tenant);
		station.setExceptions(List.of(exception));
		var businessHour = createBusinessPeriod();
		businessHour.setStartTime(new Timestamp(500));
		businessHour.setEndTime(new Timestamp(3000));
		station.setOpeningHours(Set.of(businessHour));
		var nxOpHourDto = new NexOpeningHourRes(id, station.getId(), businessHour.getStartTime());

		Mockito.lenient().doReturn(exception).when(exceptionService)
				.findNextExceptionByStationId(timestamp, station.getId());
		Mockito.lenient().doReturn(null).when(exceptionService)
				.findNextExceptionByStoreId(timestamp, store.getId());
		Mockito.lenient().doReturn(null).when(exceptionService)
				.findNextExceptionByTenantId(timestamp, tenant.getId());
		Mockito.lenient().doReturn(Optional.of(businessHour)).when(businessDayRepository).findById(businessHour.getId());
		Mockito.lenient().doReturn(List.of(nxOpHourDto)).when(repository).findNextOpeningHourByStationId(timestamp, station.getId());

		assertEquals(exception.getPeriod().getStartTime(), service.getNextStatusTime(station, timestamp));
	}

	/**
	 * Should return next tenant exception start time.
	 * tenant exception period is 3000-5200 and opening hour is 6000-7000.
	 * station exception is 1000-2000. store has no exception.
	 * @param param random parameters
	 */
	@ParameterizedTest
	@ValueSource(longs = {2001, 2500})
	void shouldReturnTenantExceptionStartTime(Long param) {
		var timestamp = new Timestamp(param);
		station.setStore(store);
		store.setTenant(tenant);
		station.setExceptions(List.of(exception));
		//set tenant exception
		var tenantException = createFakeException();
		tenantException.setPeriod(createBusinessPeriod(3000L, 5200L));
		var businessHour = createBusinessPeriod();
		tenant.setExceptions(List.of(tenantException));
		// set opening hours
		businessHour.setStartTime(new Timestamp(6000));
		businessHour.setEndTime(new Timestamp(7000));
		station.setOpeningHours(Set.of(businessHour));
		var nxOpHourDto = new NexOpeningHourRes(id, station.getId(), businessHour.getStartTime());

		Mockito.lenient().doReturn(null).when(exceptionService)
				.findNextExceptionByStationId(timestamp, station.getId());
		Mockito.lenient().doReturn(null).when(exceptionService)
				.findNextExceptionByStoreId(timestamp, store.getId());
		Mockito.lenient().doReturn(tenantException).when(exceptionService)
				.findNextExceptionByTenantId(timestamp, tenant.getId());
		Mockito.lenient().doReturn(Optional.of(businessHour)).when(businessDayRepository).findById(businessHour.getId());
		Mockito.lenient().doReturn(List.of(nxOpHourDto)).when(repository).findNextOpeningHourByStationId(timestamp, station.getId());

		assertEquals(tenantException.getPeriod().getStartTime(), service.getNextStatusTime(station, timestamp));
	}

	/**
	 * Should return station exception close time.
	 * station exception period is 3000-5000 and opening hour is 2000-7000.
	 * store exception is 1000-2000 (and repository will not return it). tenant has no exception.
	 * @param param random parameters
	 */
	@ParameterizedTest
	@ValueSource(longs = {3001, 3500, 4800})
	void shouldReturnStationExceptionCloseTime(Long param) {
		var timestamp = new Timestamp(param);
		station.setStore(store);
		store.setTenant(tenant);
		store.setExceptions(List.of(exception));
		//set station exception
		var stationException = createFakeException();
		stationException.setPeriod(createBusinessPeriod(3000L, 5000L));
		station.setExceptions(List.of(stationException));
		// set opening hours
		var businessHour = createBusinessPeriod();
		businessHour.setStartTime(new Timestamp(2000));
		businessHour.setEndTime(new Timestamp(7000));
		station.setOpeningHours(Set.of(businessHour));
		var nxOpHourDto = new NexOpeningHourRes(id, station.getId(), new Timestamp(9000));

		Mockito.lenient().doReturn(stationException).when(exceptionService)
				.findNextExceptionByStationId(timestamp, station.getId());
		Mockito.lenient().doReturn(null).when(exceptionService)
				.findNextExceptionByStoreId(timestamp, store.getId());
		Mockito.lenient().doReturn(null).when(exceptionService)
				.findNextExceptionByTenantId(timestamp, tenant.getId());
		Mockito.lenient().doReturn(Optional.of(businessHour)).when(businessDayRepository).findById(businessHour.getId());
		Mockito.lenient().doReturn(List.of(nxOpHourDto)).when(repository).findNextOpeningHourByStationId(timestamp, station.getId());

		assertEquals(stationException.getPeriod().getEndTime(), service.getNextStatusTime(station, timestamp));
	}


	private ChargingStationEntity createFakeStation() {
		var station = new ChargingStationEntity();
		station.setOpeningHours(Set.of(createBusinessPeriod(3000L, 4000L)));
		station.setId(id);
		station.setExceptions(List.of(exception));
		return station;
	}

	private BusinessDayEntity createBusinessPeriod() {
		var entity = new BusinessDayEntity();
		entity.setId(id);
		entity.setStartTime(new Timestamp(1000));
		entity.setEndTime(new Timestamp(2000));
		return entity;
	}

	private BusinessDayEntity createBusinessPeriod(Long start, Long end) {
		var entity = new BusinessDayEntity();
		entity.setId(id);
		entity.setStartTime(new Timestamp(start));
		entity.setEndTime(new Timestamp(end));
		return entity;
	}

	private ExceptionEntity createFakeException() {
		var entity = new ExceptionEntity();
		entity.setId(id);
		entity.setStatus(ExceptionEntity.Status.OPEN);
		entity.setPeriod(period);
		return entity;
	}

	private StoreEntity createFakeStore() {
		var store = new StoreEntity();
		store.setId(id);
		store.setChargingStations(List.of(station));
		return store;
	}

	private TenantEntity createFakeTenant() {
		var tenant = new TenantEntity();
		tenant.setId(id);
		return tenant;
	}


}
package com.example.htbchallenge.persistance.service;

import com.example.htbchallenge.common.exceptions.ResourceNotFoundException;
import com.example.htbchallenge.persistance.entity.BusinessDayEntity;
import com.example.htbchallenge.persistance.entity.ExceptionEntity;
import com.example.htbchallenge.persistance.entity.query.NextExceptionRes;
import com.example.htbchallenge.persistance.repository.ExceptionRepository;
import com.example.htbchallenge.persistance.service.exception.ExceptionService;
import com.example.htbchallenge.persistance.service.exception.ExceptionsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Hadi Najafi
 */

@ExtendWith(MockitoExtension.class)
public class ExceptionServiceTest {

	@Mock
	private ExceptionRepository repository;
	private ExceptionService service;
	private ExceptionEntity exceptionEntity;
	private final UUID id = UUID.randomUUID();

	@BeforeEach
	void init() {
		exceptionEntity = createFakeException();
		service = new ExceptionsServiceImpl(repository);
		Mockito.lenient().doReturn(Optional.of(exceptionEntity)).when(repository).findById(id);
	}

	@Test
	void fetchNotExistedStationShouldThrowResourceNotFoundException() {
		Mockito.lenient().doReturn(Optional.empty()).when(repository).findById(id);
		assertThrows(ResourceNotFoundException.class, () -> service.fetch(id));
	}

	@Test
	void fetchExistingEntityShouldWork() {
		assertDoesNotThrow(() -> service.fetch(id));
	}

	@ParameterizedTest
	@ValueSource(longs = {4000, 2000, 4500, 4001, 1999, 1000, 0})
	void checkStatusWithInvalidInputShouldReturnNull(Long timestamp) {
		var status = service.checkStatus(id, new Timestamp(timestamp));
		assertNull(status);
	}

	@ParameterizedTest
	@ValueSource(longs = {2500, 2001, 3999, 3000})
	void checkStatusWithValidInputShouldReturnOpen(Long timestamp) {
		var status = service.checkStatus(id, new Timestamp(timestamp));
		assertEquals(ExceptionEntity.Status.OPEN, status);
	}

	@Test
	void findNextExceptionByStationTest() {
		var time = new Timestamp(System.currentTimeMillis());
		var id = UUID.randomUUID();
		var result = new NextExceptionRes(exceptionEntity.getId(), time);
		Mockito.lenient().doReturn(List.of(result)).when(repository).findNextExceptionByStationId(time, id);
		assertEquals(exceptionEntity, service.findNextExceptionByStationId(time, id));
	}

	@Test
	void findNextExceptionByStore() {
		var time = new Timestamp(System.currentTimeMillis());
		var id = UUID.randomUUID();
		var result = new NextExceptionRes(exceptionEntity.getId(), time);
		Mockito.lenient().doReturn(List.of(result)).when(repository).findNextExceptionByStoreId(time, id);
		assertEquals(exceptionEntity, service.findNextExceptionByStoreId(time, id));
	}

	@Test
	void findNextExceptionByTenantTest() {
		var time = new Timestamp(System.currentTimeMillis());
		var id = UUID.randomUUID();
		var result = new NextExceptionRes(exceptionEntity.getId(), time);
		Mockito.lenient().doReturn(List.of(result)).when(repository).findNextExceptionByTenantId(time, id);
		assertEquals(exceptionEntity, service.findNextExceptionByTenantId(time, id));
	}

	private ExceptionEntity createFakeException() {
		var entity = new ExceptionEntity();
		entity.setId(id);
		entity.setStatus(ExceptionEntity.Status.OPEN);
		entity.setPeriod(createBusinessPeriod());
		return entity;
	}

	private BusinessDayEntity createBusinessPeriod() {
		var entity = new BusinessDayEntity();
		entity.setStartTime(new Timestamp(2000L));
		entity.setEndTime(new Timestamp(4000L));
		return entity;
	}
}

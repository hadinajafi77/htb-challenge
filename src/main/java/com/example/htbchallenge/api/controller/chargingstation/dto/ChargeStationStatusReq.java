package com.example.htbchallenge.api.controller.chargingstation.dto;

import lombok.Data;

import java.sql.Timestamp;

/**
 * @author Hadi Najafi
 */

@Data
public class ChargeStationStatusReq {
	private Timestamp timestamp;
}

package com.example.htbchallenge.api.controller.base;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Hadi Najafi
 *
 * Response structure for returning from APIs. Improves consistency for error responses.
 */
@Getter
public class RestErrorResponse {
	private String key;
	private List<Object> messages = new ArrayList<>();

	public RestErrorResponse(String key, List<?> messages) {
		this.key = key;
		this.messages.addAll(messages);
	}
}

package com.example.htbchallenge.api.controller;

import com.example.htbchallenge.api.controller.base.RestErrorResponse;
import com.example.htbchallenge.common.exceptions.GlobalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


/**
 * @author Hadi Najafi
 */
@ControllerAdvice
public class GlobalExceptionHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@ExceptionHandler(GlobalException.class)
	public ResponseEntity<RestErrorResponse> handleException(GlobalException ex) {
		LOGGER.error("", ex);
		return new ResponseEntity<>(new RestErrorResponse(ex.getKey(), ex.getMessages()), ex.getStatus());
	}


}

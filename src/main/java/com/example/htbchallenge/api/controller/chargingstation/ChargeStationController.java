package com.example.htbchallenge.api.controller.chargingstation;

import com.example.htbchallenge.api.controller.chargingstation.dto.ChargeStationStatusReq;
import com.example.htbchallenge.persistance.entity.ChargingStationEntity;
import com.example.htbchallenge.persistance.entity.ExceptionEntity;
import com.example.htbchallenge.persistance.service.chargingstation.ChargingStationService;
import com.example.htbchallenge.persistance.service.exception.ExceptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static com.example.htbchallenge.common.Constants.PathUrls.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Hadi Najafi
 */

@RestController
@RequestMapping(path = V1 + CHARGING_STATION_BASE, produces = APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class ChargeStationController {
	private final ChargingStationService stationService;
	private final ExceptionService exceptionService;

	/**
	 *
	 * @param id station id.
	 * @param req Request body which contains required timestamp.
	 * @return Current station status.
	 */
	@PostMapping(path = "/{id}", consumes = APPLICATION_JSON_VALUE)
	public ChargingStationEntity.Status currentStationStatus(@PathVariable("id") final String id,
															 @RequestBody final ChargeStationStatusReq req) {
		//validation part: request body fields need to be validated

		final var station = stationService.fetch(UUID.fromString(id));
		var stationExceptions = station.getExceptions();
		var status = getStationStatusByExceptions(stationExceptions, req.getTimestamp());
		if (status != null)
			return status;
		//check store exceptions
		var storeExceptions = station.getStore().getExceptions();
		status = getStationStatusByExceptions(storeExceptions, req.getTimestamp());
		if (status != null)
			return status;
		//check tenant exceptions
		var tenantExceptions = station.getStore().getTenant().getExceptions();
		status = getStationStatusByExceptions(tenantExceptions, req.getTimestamp());
		if (status != null)
			return status;
		// check opening hours at last
		return stationService.checkOpeningHour(station, req.getTimestamp());
	}

	private ChargingStationEntity.Status getStationStatusByExceptions(List<ExceptionEntity> exceptions, Timestamp time) {
		if (exceptions != null) {
			var stationExceptionStatus = getExceptionStatusByExceptions(exceptions, time);
			if (stationExceptionStatus != null) {
				if (stationExceptionStatus.equals(ExceptionEntity.Status.OPEN))
					return ChargingStationEntity.Status.OPEN;
				return ChargingStationEntity.Status.CLOSED;
			}
		}
		return null;
	}

	private ExceptionEntity.Status getExceptionStatusByExceptions(List<ExceptionEntity> exceptions, Timestamp time) {
		return exceptions.stream().map(exc -> exceptionService.checkStatus(exc.getId(), time))
				.filter(Objects::nonNull).findFirst().orElse(null);
	}

	/**
	 *
	 * @param id station id.
	 * @param req Request body which contains required timestamp.
	 * @return Next Open/Close occurrence.
	 */
	@PostMapping(path = "/{id}" + NEXT_STATUS, consumes = APPLICATION_JSON_VALUE)
	public Timestamp nextStatusOccurrence(@PathVariable("id") final String id,
												   @RequestBody ChargeStationStatusReq req) {
		//validation should happen here

		var station = stationService.fetch(UUID.fromString(id));
		return stationService.getNextStatusTime(station, req.getTimestamp());
	}

	/*
	 * Controller APIs could be better:
	 * Instead of returning just status, we could return the whole ChargingStation information.
	 * We have 2 APIs which can be just one, and having both of them (status & next event) as the response body.
	 *
	 * Next status timestamp doesn't give much information about the station status itself. it
	 * just shows the next event.
	 *
	 * We don't have any validation right now, but checking input to be not null, correct format
	 * and correct data type should be checked.
	 *
	 */

}

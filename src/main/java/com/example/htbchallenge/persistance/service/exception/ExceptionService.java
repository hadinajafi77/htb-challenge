package com.example.htbchallenge.persistance.service.exception;

import com.example.htbchallenge.persistance.entity.ExceptionEntity;

import java.sql.Timestamp;
import java.util.UUID;

/**
 * @author Hadi Najafi
 */

public interface ExceptionService {
	ExceptionEntity fetch(UUID id);

	ExceptionEntity.Status checkStatus(UUID id, Timestamp timestamp);

	ExceptionEntity findNextExceptionByStationId(Timestamp timestamp, UUID stationId);

	ExceptionEntity findNextExceptionByStoreId(Timestamp timestamp, UUID storeId);

	ExceptionEntity findNextExceptionByTenantId(Timestamp timestamp, UUID tenantId);
}

package com.example.htbchallenge.persistance.service.exception;

import com.example.htbchallenge.common.Constants;
import com.example.htbchallenge.common.exceptions.ResourceNotFoundException;
import com.example.htbchallenge.persistance.entity.ExceptionEntity;
import com.example.htbchallenge.persistance.repository.ExceptionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.UUID;

import static java.text.MessageFormat.format;

/**
 * @author Hadi Najafi
 */

@Service
@Transactional
@RequiredArgsConstructor
public class ExceptionsServiceImpl implements ExceptionService {
	private final ExceptionRepository repository;

	@Override
	public ExceptionEntity fetch(UUID id) {
		final var optional = repository.findById(id);
		if (optional.isEmpty())
			throw new ResourceNotFoundException(format(Constants.ErrorMsg.MSG_NOT_FOUND, "Exception", id));
		return optional.get();
	}

	@Override
	public ExceptionEntity.Status checkStatus(UUID id, Timestamp timestamp) {
		final var exception = fetch(id);
		if (exception.getPeriod().isInPeriod(timestamp)) {
			return exception.getStatus();
		}
		return null;
	}

	@Override
	public ExceptionEntity findNextExceptionByStationId(Timestamp timestamp, UUID stationId) {
		return repository.findNextExceptionByStationId(timestamp, stationId)
				.stream().findFirst().map(obj -> fetch(obj.getExceptionId())).orElse(null);
	}

	@Override
	public ExceptionEntity findNextExceptionByStoreId(Timestamp timestamp, UUID storeId) {
		return repository.findNextExceptionByStoreId(timestamp, storeId)
				.stream().findFirst().map(obj -> fetch(obj.getExceptionId())).orElse(null);
	}

	@Override
	public ExceptionEntity findNextExceptionByTenantId(Timestamp timestamp, UUID tenantId) {
		return repository.findNextExceptionByTenantId(timestamp, tenantId)
				.stream().findFirst().map(obj -> fetch(obj.getExceptionId())).orElse(null);
	}
}

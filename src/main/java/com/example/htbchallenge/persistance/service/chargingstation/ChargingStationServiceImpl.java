package com.example.htbchallenge.persistance.service.chargingstation;

import com.example.htbchallenge.common.Constants;
import com.example.htbchallenge.common.exceptions.ResourceNotFoundException;
import com.example.htbchallenge.persistance.entity.ChargingStationEntity;
import com.example.htbchallenge.persistance.entity.ChargingStationEntity.Status;
import com.example.htbchallenge.persistance.entity.ExceptionEntity;
import com.example.htbchallenge.persistance.repository.BusinessDayRepository;
import com.example.htbchallenge.persistance.repository.ChargingStationRepository;
import com.example.htbchallenge.persistance.service.exception.ExceptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static java.text.MessageFormat.format;

/**
 * @author Hadi Najafi
 */

@Service
@Transactional
@RequiredArgsConstructor
public class ChargingStationServiceImpl implements ChargingStationService {
	private final ChargingStationRepository repository;
	private final ExceptionService exceptionService;
	private final BusinessDayRepository businessDayRepository;

	@Override
	public Status checkOpeningHour(ChargingStationEntity station, Timestamp timestamp) {
		var openingHours = station.getOpeningHours();
		for (var day : openingHours) {
			if (day.isInPeriod(timestamp)) {
				return Status.OPEN;
			}
		}
		return Status.CLOSED;
	}

	@Override
	public ChargingStationEntity fetch(UUID id) {
		var optional = repository.findById(id);
		if (optional.isEmpty())
			throw new ResourceNotFoundException(format(Constants.ErrorMsg.MSG_NOT_FOUND, "Station", id));
		return optional.get();
	}

	@Override
	public Timestamp getNextStatusTime(ChargingStationEntity station, Timestamp time) {
		var nextStatusTimes = new ArrayList<Long>();

		Timestamp result = nextStationException(time, station);
		if (result != null)
			nextStatusTimes.add(result.getTime());

		result = nextStoreException(time, station);
		if (result != null)
			nextStatusTimes.add(result.getTime());

		result = nextTenantException(time, station);
		if (result != null)
			nextStatusTimes.add(result.getTime());

		result = nextStationOpenCloseTime(time, station);
		if (result != null)
			nextStatusTimes.add(result.getTime());

		Collections.sort(nextStatusTimes);
		return new Timestamp(nextStatusTimes.get(0));

	}

	private Timestamp nextStationException(Timestamp time, ChargingStationEntity station) {
		if (station.getExceptions() != null) {
			var inPeriod = getPeriodEndTime(station.getExceptions(), time);
			if (inPeriod != null)
				return inPeriod;
		}
		var nextException =  exceptionService
				.findNextExceptionByStationId(time, station.getId());
		if (nextException != null)
			return nextException.getPeriod().getStartTime();
		return null;
	}

	private Timestamp getPeriodEndTime(List<ExceptionEntity> exceptions, Timestamp time) {
		for (var exception : exceptions) {
			if (exception.getPeriod().isInPeriod(time))
				return exception.getPeriod().getEndTime();
		}
		return null;
	}

	private Timestamp nextStoreException(Timestamp time, ChargingStationEntity station) {
		if (station.getStore().getExceptions() != null) {
			var inPriod = getPeriodEndTime(station.getStore().getExceptions(), time);
			if (inPriod != null)
				return inPriod;
		}
		var nextException =  exceptionService
				.findNextExceptionByStoreId(time, station.getStore().getId());
		if (nextException != null)
			return nextException.getPeriod().getStartTime();
		return null;
	}

	private Timestamp nextTenantException(Timestamp time, ChargingStationEntity station) {
		if (station.getStore().getTenant().getExceptions() != null) {
			var inPeriod = getPeriodEndTime(station.getStore().getTenant().getExceptions(), time);
			if (inPeriod != null)
				return inPeriod;
		}
		var nextException = exceptionService
				.findNextExceptionByTenantId(time, station.getStore().getTenant().getId());
		if (nextException != null)
			return nextException.getPeriod().getStartTime();
		return null;
	}

	private Timestamp nextStationOpenCloseTime(Timestamp time, ChargingStationEntity station) {
		for (var period : station.getOpeningHours()) {
			if (period.isInPeriod(time)) {
				return period.getEndTime();
			}
		}
		return repository.findNextOpeningHourByStationId(time, station.getId())
				.stream().findFirst().map(obj -> businessDayRepository.findById(obj.getBusinessDayId()).get()
						.getStartTime()).orElse(null);
	}
}

package com.example.htbchallenge.persistance.service.chargingstation;

import com.example.htbchallenge.persistance.entity.ChargingStationEntity;

import java.sql.Timestamp;
import java.util.UUID;

/**
 * @author Hadi Najafi
 */

public interface ChargingStationService {

	ChargingStationEntity fetch(UUID id);

	ChargingStationEntity.Status checkOpeningHour(ChargingStationEntity station, Timestamp timestamp);

	Timestamp getNextStatusTime(ChargingStationEntity station, Timestamp time);
}

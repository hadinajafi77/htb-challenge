package com.example.htbchallenge.persistance.repository;

import com.example.htbchallenge.persistance.entity.ExceptionEntity;
import com.example.htbchallenge.persistance.entity.query.NextExceptionRes;
import org.springframework.data.jpa.repository.Query;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

/**
 * @author Hadi Najafi
 */

public interface ExceptionRepository extends BaseRepository<ExceptionEntity> {

	@Query("SELECT new com.example.htbchallenge.persistance.entity.query.NextExceptionRes(e.id, MIN (e.period.startTime)) FROM ExceptionEntity e GROUP BY e.id, " +
			"e.period.startTime, e.station.id HAVING e.period.startTime > ?1 AND e.station.id = ?2 ORDER BY MIN (e.period.startTime) ASC")
	List<NextExceptionRes> findNextExceptionByStationId(Timestamp time, UUID stationId);

	@Query("SELECT new com.example.htbchallenge.persistance.entity.query.NextExceptionRes(e.id, MIN (e.period.startTime)) FROM ExceptionEntity e GROUP BY e.id, " +
			"e.period.startTime, e.store.id HAVING e.period.startTime > ?1 AND e.store.id = ?2 ORDER BY MIN (e.period.startTime) ASC")
	List<NextExceptionRes> findNextExceptionByStoreId(Timestamp time, UUID storeId);

	@Query("SELECT new com.example.htbchallenge.persistance.entity.query.NextExceptionRes(e.id, MIN (e.period.startTime)) FROM ExceptionEntity e GROUP BY e.id, " +
			"e.period.startTime, e.tenant.id HAVING e.period.startTime > ?1 AND e.tenant.id = ?2 ORDER BY MIN (e.period.startTime) ASC")
	List<NextExceptionRes> findNextExceptionByTenantId(Timestamp time, UUID tenantId);
}

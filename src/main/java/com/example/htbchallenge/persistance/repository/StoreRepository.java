package com.example.htbchallenge.persistance.repository;

import com.example.htbchallenge.persistance.entity.StoreEntity;

/**
 * @author Hadi Najafi
 */

public interface StoreRepository extends BaseRepository<StoreEntity> {
}

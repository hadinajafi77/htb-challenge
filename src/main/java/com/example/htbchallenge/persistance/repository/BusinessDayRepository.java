package com.example.htbchallenge.persistance.repository;

import com.example.htbchallenge.persistance.entity.BusinessDayEntity;

/**
 * @author Hadi Najafi
 */

public interface BusinessDayRepository extends BaseRepository<BusinessDayEntity> {

}

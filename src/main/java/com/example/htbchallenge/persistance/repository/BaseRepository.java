package com.example.htbchallenge.persistance.repository;

import com.example.htbchallenge.persistance.entity.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * @author Hadi Najafi
 * <p>
 * Base repository of every repository created for accessing datasource
 */

@NoRepositoryBean
@Transactional
public interface BaseRepository<T extends BaseEntity> extends JpaRepository<T, UUID>, JpaSpecificationExecutor<T> {
}

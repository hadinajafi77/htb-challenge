package com.example.htbchallenge.persistance.repository;

import com.example.htbchallenge.persistance.entity.TenantEntity;

/**
 * @author Hadi Najafi
 */

public interface TenantRepository extends BaseRepository<TenantEntity> {
}

package com.example.htbchallenge.persistance.repository;

import com.example.htbchallenge.persistance.entity.ChargingStationEntity;
import com.example.htbchallenge.persistance.entity.query.NexOpeningHourRes;
import org.springframework.data.jpa.repository.Query;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

/**
 * @author Hadi Najafi
 */

public interface ChargingStationRepository extends BaseRepository<ChargingStationEntity> {
	@Query("SELECT new com.example.htbchallenge.persistance.entity.query.NexOpeningHourRes( b.id, s.id, MIN(b.startTime)) " +
			"FROM ChargingStationEntity s INNER JOIN BusinessDayEntity b on s.id = b.station.id " +
			"GROUP BY b.id, s.id, b.startTime HAVING b.startTime > ?1 AND s.id = ?2 ORDER BY MIN(b.startTime) ASC ")
	List<NexOpeningHourRes> findNextOpeningHourByStationId(Timestamp time, UUID stationId);
}

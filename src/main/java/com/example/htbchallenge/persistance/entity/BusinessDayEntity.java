package com.example.htbchallenge.persistance.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @author Hadi Najafi
 */

@Entity
@Table
@Getter
@Setter
public class BusinessDayEntity extends BaseEntity {

	public enum Type {
		BASIC,
		EMPLOYEES,
		MALL
	}

	@Column
	private Timestamp startTime;
	@Column
	private Timestamp endTime;
	@Column
	@Enumerated(EnumType.STRING)
	private Type type;

	@OneToOne(mappedBy = "period")
	private ExceptionEntity exception;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_station_id", referencedColumnName = "pk_id")
	private ChargingStationEntity station;

	public boolean isInPeriod(Timestamp item) {
		return item.after(startTime) && item.before(endTime);
	}

}

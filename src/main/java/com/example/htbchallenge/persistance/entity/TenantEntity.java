package com.example.htbchallenge.persistance.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * @author Hadi Najafi
 */

@Entity
@Table(name = "htb_tenant")
@Setter
@Getter
public class TenantEntity extends BaseEntity {
	@Column
	private String name;
	@Column
	private String email;
	@Column(name = "expired", columnDefinition = "boolean default false")
	private Boolean expired = false;

	@OneToMany(mappedBy = "tenant", orphanRemoval = true, cascade = CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.EXTRA)
	private List<StoreEntity> store;

	@OneToMany(mappedBy = "tenant")
	@LazyCollection(LazyCollectionOption.EXTRA)
	private List<ExceptionEntity> exceptions;
}

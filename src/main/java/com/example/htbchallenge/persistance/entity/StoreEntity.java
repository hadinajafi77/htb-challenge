package com.example.htbchallenge.persistance.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * @author Hadi Najafi
 */

@Entity
@Table(name = "htb_store")
@Setter
@Getter
public class StoreEntity extends BaseEntity {
	@Column
	private String title;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_tenant_id", referencedColumnName = "pk_id")
	private TenantEntity tenant;

	@OneToMany(mappedBy = "store", orphanRemoval = true)
	@LazyCollection(LazyCollectionOption.EXTRA)
	private List<ChargingStationEntity> chargingStations;

	@OneToMany(mappedBy = "store")
	@LazyCollection(LazyCollectionOption.EXTRA)
	private List<ExceptionEntity> exceptions;

}

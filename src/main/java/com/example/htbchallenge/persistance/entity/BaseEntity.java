package com.example.htbchallenge.persistance.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

/**
 * @author Hadi Najafi
 *
 * Base class of every entity in the system
 */

@MappedSuperclass
@Getter
@Setter
public class BaseEntity {
	@Id
	@Column(name = "pk_id")
	protected UUID id;

}

package com.example.htbchallenge.persistance.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author Hadi Najafi
 */

@Entity
@Table
@Getter
@Setter
public class ExceptionEntity extends BaseEntity {

	public enum Status {
		OPEN, CLOSED
	}

	@Column
	private String description;
	@Column
	private Status status;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_period_id")
	private BusinessDayEntity period;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_store_id", referencedColumnName = "pk_id")
	private StoreEntity store;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_tenant_id", referencedColumnName = "pk_id")
	private TenantEntity tenant;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_station_id", referencedColumnName = "pk_id")
	private ChargingStationEntity station;
}

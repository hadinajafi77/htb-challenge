package com.example.htbchallenge.persistance.entity.query;

import lombok.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

/**
 * @author Hadi Najafi
 */

@Setter
@Getter
@NoArgsConstructor
public class NextExceptionRes {
	private UUID exceptionId;
	private Timestamp minTime;

	public NextExceptionRes(UUID exceptionId, Date minTime) {
		this.exceptionId = exceptionId;
		this.minTime = new Timestamp(minTime.getTime());
	}
}


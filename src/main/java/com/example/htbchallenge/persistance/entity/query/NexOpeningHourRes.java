package com.example.htbchallenge.persistance.entity.query;

import lombok.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

/**
 * @author Hadi Najafi
 */

@Setter
@Getter
@NoArgsConstructor
public class NexOpeningHourRes {
	private UUID businessDayId;
	private UUID stationId;
	private Timestamp minStartTime;

	public NexOpeningHourRes(UUID businessDayId, UUID stationId, Date minStartTime) {
		this.businessDayId = businessDayId;
		this.stationId = stationId;
		this.minStartTime = new Timestamp(minStartTime.getTime());
	}
}

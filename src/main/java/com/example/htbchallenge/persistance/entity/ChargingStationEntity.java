package com.example.htbchallenge.persistance.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * @author Hadi Najafi
 */

@Entity
@Table(name = "htb_charging_station")
@Setter
@Getter
public class ChargingStationEntity extends BaseEntity {

	public enum Status {
		OPEN, CLOSED
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_store_id", referencedColumnName = "pk_id")
	private StoreEntity store;

	@OneToMany(mappedBy = "station")
	@LazyCollection(LazyCollectionOption.EXTRA)
	private Set<BusinessDayEntity> openingHours;

	@OneToMany(mappedBy = "station")
	@LazyCollection(LazyCollectionOption.EXTRA)
	private List<ExceptionEntity> exceptions;

}

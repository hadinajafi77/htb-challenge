package com.example.htbchallenge.common.exceptions;

import org.springframework.http.HttpStatus;

import static com.example.htbchallenge.common.Constants.ErrorMsg.ERROR_NOT_FOUND;

/**
 * @author Hadi Najafi
 */

public class ResourceNotFoundException extends GlobalException {
	public ResourceNotFoundException(String message) {
		super(ERROR_NOT_FOUND, HttpStatus.NOT_FOUND);
		this.getMessages().add(message);
	}
}

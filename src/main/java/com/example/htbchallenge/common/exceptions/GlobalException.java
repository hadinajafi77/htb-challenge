package com.example.htbchallenge.common.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Hadi Najafi
 */
@Getter
@Setter
public abstract class GlobalException extends RuntimeException {
    private final String key;
    private final HttpStatus status;
    private final List<Object> messages = new ArrayList<>();

    public GlobalException(String key, HttpStatus status) {
        this.key = key;
        this.status = status;
    }

}

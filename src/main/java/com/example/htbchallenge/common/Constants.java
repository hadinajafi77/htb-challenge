package com.example.htbchallenge.common;

/**
 * @author Hadi Najafi
 * Contains all the constants used in the entire project
 * Constants are categorized by subclasses
 * This class is final and is not instantiable
 */

public final class Constants {
	private Constants() {}

	public static final class PathUrls {
		private PathUrls() {}
		public static final String V1 = "/v1";
		public static final String CHARGING_STATION_BASE = "/api/charging-station";
		public static final String NEXT_STATUS = "/next-status";
	}

	public static final class ErrorMsg {
		private ErrorMsg() {}
		public static final String ERROR_NOT_FOUND = "not_found_error";
		public static final String ERROR_CLIENT = "client_error";

		public static final String MSG_NOT_FOUND = "{1} with id {2} not found";
	}

}

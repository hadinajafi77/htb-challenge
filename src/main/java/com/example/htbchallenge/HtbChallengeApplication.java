package com.example.htbchallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HtbChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(HtbChallengeApplication.class, args);
	}

}
